import fr.afpa.banque.controller.AgenceController;
import fr.afpa.banque.controller.ClientController;
import fr.afpa.banque.controller.CompteBancaireController;
import fr.afpa.banque.service.CompteBancaireService;
import fr.afpa.banque.util.Constante;
import fr.afpa.banque.views.AppView;
import org.postgresql.gss.GSSOutputStream;


public class Main {


    private static AppView appView = new AppView();

    public static void main(String[] args) {

        AgenceController agenceController = new AgenceController();
        ClientController clientController = new ClientController();
        CompteBancaireController compteBancaireController = new CompteBancaireController();


        String choice= "";
        String subChoice="";

        do {
            choice = appView.menu();

            switch (choice) {
                case "1":
                    agenceController.create();
                    break;
                case "2":
                    clientController.menuCreerClient();
                    break;
                case "3":
                    compteBancaireController.menuCreateCompte();
                    break;
                case "4":
                    compteBancaireController.findCompte();
                    break;
                case "5":
                    subChoice = appView.subMenu();
                    switch(subChoice){
                        case "A":
                            clientController.affichageClient(clientController.findByName());
                            break;
                        case"B":
                            clientController.affichageUniqueClient(compteBancaireController.findClientByIdClient());
                            break;
                        case"C":
                            clientController.affichageClient(clientController.findClientByIdClient());
                            break;
                    }
                    break;
                case "6":
                    compteBancaireController.afficherComptesBancaires(compteBancaireController.findCompteByIdClient());
                    break;
                case "7":
                    clientController.printInfosClient();
                    break;
                case "8":
                    System.out.println("quitter l'application ");
                    break;
                default:
                    break;
            }
        } while (!choice.equals("8"));
    }
}

