package fr.afpa.banque.util;

import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Constante {

    public static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static Scanner scanner = new Scanner(System.in);
    public final static String CC= "COMPTE_COURANT";
    public final static String PEL= "PLAN_EPARGNE_LOGEMENT";
    public final static String LA= "LIVRET_A";

}
