package fr.afpa.banque.controller;

import fr.afpa.banque.model.Agence;
import fr.afpa.banque.service.AgenceService;
import fr.afpa.banque.views.AgenceView;


public class AgenceController {
	public AgenceService agenceService= new AgenceService();
	public AgenceView agenceView = new AgenceView();
	public AgenceController(){
	}

	public void create(){
		String[] agenceTab = agenceView.create();
		Agence agence = new Agence();

		agence.setCode(agenceTab[0]);
		agence.setNom(agenceTab[1]);
		agence.setAdresse(agenceTab[2]);

		agenceService.create(agence);
	}



}