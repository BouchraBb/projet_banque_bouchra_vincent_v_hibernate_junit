package fr.afpa.banque.controller;

import fr.afpa.banque.model.Agence;
import fr.afpa.banque.model.Client;
import fr.afpa.banque.model.CompteBancaire;
import fr.afpa.banque.service.AgenceService;
import fr.afpa.banque.service.ClientService;
import fr.afpa.banque.service.CompteBancaireService;
import fr.afpa.banque.util.Constante;
import fr.afpa.banque.views.AgenceView;
import fr.afpa.banque.views.AppView;
import fr.afpa.banque.views.ClientView;
import fr.afpa.banque.views.CompteBancaireView;

import java.util.List;

public class CompteBancaireController {
    private static AppView appView = new AppView();
    CompteBancaireService compteBancaireService = new CompteBancaireService();
    ClientView clientView = new ClientView();
    ClientService clientService= new ClientService();
     AgenceService agenceService= new AgenceService();
     AgenceView agenceView = new AgenceView();

    CompteBancaireView compteBancaireView = new CompteBancaireView();


    public void menuCreateCompte() {
        String codeAgence = agenceView.promptCodeAgence();
        Agence agence = agenceService.find(codeAgence);

        String idClient = clientView.promptIdClient();
        Client client = clientService.find(idClient);
        String type;

        boolean possede = false ;
        do{
             CompteBancaire compte = null;
            do{
                type = compteBancaireView.recupererTypeCompte();
            }
            while(type==null);

            compte = compteBancaireService.findeCompteSameType(client, type);
            if(compte!= null) {
                possede= true;
                appView.showMessage(" le client possède déja d'un compte du type " + type);
            }
        }while(possede);

            Boolean decouvert;
        do{
            decouvert = compteBancaireView.decouvertAutorise();
        }while(decouvert==null);

        double solde = compteBancaireView.recupererSolde();
        solde =calculeFrais(type, solde );

        CompteBancaire compteBancaire = new CompteBancaire(agence, client,type, solde, decouvert);
        compteBancaireService.create(compteBancaire);

    }

    private double calculeFrais(String type, double solde ) {
        double soldeMoinFrais= 0.0;
        if(type.equals(Constante.CC)) soldeMoinFrais = solde - 25.0;
        else if(type.equals(Constante.PEL)) soldeMoinFrais = solde - (solde*0.025);
        else soldeMoinFrais  = solde - (solde*0.1);
         return  soldeMoinFrais;
    }

    public void findCompte(){
       String num = compteBancaireView.recupererNumeroCompteBancaire();
       CompteBancaire compteBancaire= compteBancaireService.find(num);
       compteBancaireView.afficherCompte(compteBancaire);
    }

    public Client findClientByIdClient(){
        String idClient = compteBancaireService.findIdClientByNumeroDeCompte();
        Client client = clientService.findByIdString(idClient);
        return client;
    }

    public List<CompteBancaire> findCompteByIdClient(){
        System.out.println("Veuillez indiquer un id : ");
        String idClient = Constante.scanner.nextLine();
        return compteBancaireService.findCompteByIdClient(idClient);
    }

    public void afficherComptesBancaires(List<CompteBancaire> comptesBancaires) {
        compteBancaireView.afficherComptesBancaires(comptesBancaires);
    }
}
