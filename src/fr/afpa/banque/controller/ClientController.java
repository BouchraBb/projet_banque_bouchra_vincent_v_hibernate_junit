package fr.afpa.banque.controller;
import fr.afpa.banque.service.ClientService;
import fr.afpa.banque.util.Constante;
import fr.afpa.banque.views.ClientView;
import fr.afpa.banque.model.Client;

import java.sql.Date;
import java.util.List;


public class ClientController {
	 ClientView clientView = new ClientView();
	 ClientService clientService= new ClientService();

	public void menuCreerClient(){

		String[] infos =  clientView.recupererInfosClient();
		Client client = new Client(infos[0],infos[1], Date.valueOf(infos[3]),infos[2]);
		clientService.create(client);
	}

	public void printInfosClient(){
		String idClient = clientView.promptIdClient();
		Client client = clientService.find(idClient);
		clientView.printInfos(client);
	}
	


	public void menuRechercherClient(){
//		int choix;
//		int indexClientAAfficher;
//		choix = clientView.recupererChoix();
//		indexClientAAfficher = clientView.recupererElementClient(choix, clients, comptesBancaires);
//		clientDao.rechercherElementClient(clients, comptesBancaires, choix, indexClientAAfficher);
	}
	
	
	public void menuAffichageListe(){
		List<Client> clients = clientService.findAll();
		clientView.afficherAllClient(clients);
	}

	public List<Client> findByName(){
		System.out.println("Veuillez indiquer le nom recherché : ");
		String name = Constante.scanner.nextLine();
		return clientService.findByName(name);
	}

	public List<Client> findByNumeroDeCompte(){
		System.out.println("Veuillez indiquer le numero de compte recherché : ");
		String numeroCompte = Constante.scanner.nextLine();
		return clientService.findByNumeroDeCompte(numeroCompte);
	}

	public List<Client> findClientByIdClient(){
		System.out.println("Veuillez indiquer l'id du client recherché : ");
		String idClient = Constante.scanner.nextLine();
		return clientService.findClientByIdClient(idClient);
	}

	public Client findByIdClient(String idClient){
		return clientService.findByIdString(idClient);
	}

	public void affichageClient(List<Client> clients){
		clientView.afficherAllClient(clients);
	}

	public void affichageUniqueClient(Client client){
		clientView.afficherClient(client);
	}

}