package fr.afpa.banque.views;
import fr.afpa.banque.model.Client;
import fr.afpa.banque.util.Constante;
import fr.afpa.banque.util.ExceptionUtils;

import java.util.List;

public class ClientView {

	public String recupererIdClient(){
		System.out.println("Veuillez saisir l'Id du client");
		String id =Constante.scanner.nextLine();
		id=ExceptionUtils.checkValue(id,Constante.scanner);
		return id; 
	}

	public int recupererChoix(){

		String choix;
		int choixClient;
		boolean badChoice = false;
		do{
			System.out.println("Par quel element souhaitez-vous chercher ? \n \n" +
					"Taper: \n \n" +
					"1 =>  Rechercher par nom " + "\n" +
					"2 =>  Rechercher par numero de compte  " + "\n" +
					"3 =>  Rechercher par identifiant de client  " + "\n");
			choix = Constante.scanner.nextLine();
			choix=ExceptionUtils.checkValue(choix,Constante.scanner);
			choixClient = Integer.parseInt(choix);

			if((choixClient!=1) && (choixClient!=2) && (choixClient!=3)){
				badChoice = true;
			}
			if(badChoice == true){
				System.out.println("\n [CHOIX INCORRECT] Veuillez inserer un chiffre compris entre 1 et 3 \n \n \n");
			}
		}while(badChoice == true);

		return choixClient;

	}

	public String[] recupererInfosClient() {
		String nom;
		String prenom ;
		String email ;
		String dateDeNaissance ;
		String[]infos = new String[4];
		System.out.println("Reccuperation ifos client");
		System.out.println("Veuillez entrer le nom");
		nom = Constante.scanner.nextLine();
		infos[0]=nom;


		System.out.println("Veuillez entrer prenom");
		prenom = Constante.scanner.nextLine();
		prenom=ExceptionUtils.checkValue(prenom, Constante.scanner);
		infos[1]=prenom;

		System.out.println("Veuillez entrer email");
		email =Constante.scanner.nextLine();
		email=ExceptionUtils.checkValue(email, Constante.scanner).trim();
		infos[2]=email;

		System.out.println("Veuillez entrer date de naissance (yyyy-MM-dd)");
		dateDeNaissance =Constante.scanner.nextLine();
		infos[3]=dateDeNaissance;
		return infos;
	}

	public void afficherAllClient(List<Client> clients){
		System.out.println(System.lineSeparator());

		System.out.printf("%-20s|", "Identifiant");
		System.out.printf("%-20s|", "Nom");
		System.out.printf("%-20s|", "Prenom");
		System.out.printf("%-20s|", "Date de naissance");
		System.out.printf("%-20s|", "Email");
		System.out.println();
		System.out.print("____________________________________________________________________________________________________________________________");
		System.out.println();
		for (int i=0 ; i<clients.size(); i++){

			System.out.printf("%-20s|", clients.get(i).getId());
			System.out.printf("%-20s|", clients.get(i).getNom());
			System.out.printf("%-20s|", clients.get(i).getPrenom());
			System.out.printf("%-20s|", Constante.formatter.format( clients.get(i).getDateDeNaissance().toLocalDate()) );
			System.out.printf("%-20s|", clients.get(i).getEmail());
			System.out.println();

		}
		System.out.println(System.lineSeparator());
	}

	public void afficherClient(Client client){
		System.out.println(System.lineSeparator());

		System.out.printf("|%-20s|", "Identifiant");
		System.out.printf("%-20s|", "Nom");
		System.out.printf("%-20s|", "Prenom");
		System.out.printf("%-20s|", "Date de naissance");
		System.out.printf("%-20s|", "Email");
		System.out.println();
		System.out.print("____________________________________________________________________________________________________________________________");
		System.out.println();

		System.out.printf("%-20s|", client.getId());
		System.out.printf("%-20s|", client.getNom());
		System.out.printf("%-20s|", client.getPrenom());
		System.out.printf("%-20s|", Constante.formatter.format( client.getDateDeNaissance().toLocalDate()) );
		System.out.printf("%-20s|", client.getEmail());
		System.out.println();

	}


	public String promptIdClient() {
		System.out.println("Veuilez saisir l'identifiant du client:");
		String id = Constante.scanner.nextLine();
		return id;
	}

	public void printInfos(Client client) {
		System.out.println("\t\t\tFiche client\n");
		System.out.println("Numero client : " +client.getId()+"\n");
		System.out.println("Nom : " +client.getNom()+"\n");
		System.out.println("Prenom : " +client.getPrenom()+"\n");
		System.out.println("Date de naissance : " +  Constante.formatter.format( client.getDateDeNaissance().toLocalDate()) +"\n");
		System.out.println("__________________________________________________________\n");
		System.out.println();
		String compte;
		double solde;
		System.out.println("Liste de compte\n");
		System.out.println("__________________________________________________________\n");
		System.out.println("Numero de compte                       Solde\n");
		System.out.println("__________________________________________________________\n");
		for(int i=0; i<client.getComptes().size(); i++){
				solde = client.getComptes().get(i).getSolde();
				compte = client.getComptes().get(i).getNumeroCompte();
		System.out.println(compte+ "                         " +solde+ "         " + (solde > 0 ? ":)" : ":(") +"\n");

		}
		System.out.println();

	}
}