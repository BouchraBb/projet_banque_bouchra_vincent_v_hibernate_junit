package fr.afpa.banque.views;

import fr.afpa.banque.model.Agence;
import fr.afpa.banque.util.Constante;

import java.util.List;

public class AgenceView {

	public String[] create() {
		String[] agence = new String[3];
		agence[0] = "100";
		agence[1] = "agence";
		agence[2] = "address";
		return agence;
	}

	public static void afficherAgences(List<Agence> agences){
		System.out.println(System.lineSeparator());
		System.out.printf("|%-20s|", "Index");
		System.out.printf("%-20s|", "Code");
		System.out.printf("%-20s|", "Nom");
		System.out.printf("%-20s|", "Adresse");
		System.out.println();
		System.out.print("__");
		System.out.println();
		for (int i=0 ; i<agences.size(); i++){
			System.out.printf("|%-20s|", i);
			System.out.printf("%-20s|", agences.get(i).getCode());
			System.out.printf("%-20s|", agences.get(i).getNom());
			System.out.printf("%-20s|", agences.get(i).getAdresse());
			System.out.println();

		}
		System.out.println(System.lineSeparator());
	}
	public static void afficherAgence(Agence agence){
		System.out.println(System.lineSeparator());
		System.out.printf("%|-20s|", "Code");
		System.out.printf("%-20s|", "Nom");
		System.out.printf("%-20s|", "Adresse");
		System.out.println();
		System.out.print("__");
		System.out.println();
			System.out.printf("%|-20s|", agence.getCode());
			System.out.printf("%-20s|", agence.getNom());
			System.out.printf("%-20s|", agence.getAdresse());
			System.out.println();
	}

	public String promptCodeAgence(){
		System.out.println("Veuilez saisir le code de l'agence :");
		String code = Constante.scanner.nextLine();
		return code;
	}

}