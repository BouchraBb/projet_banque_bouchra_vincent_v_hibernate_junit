package fr.afpa.banque.views;

import fr.afpa.banque.model.CompteBancaire;
import fr.afpa.banque.util.Constante;
import fr.afpa.banque.util.ExceptionUtils;

import java.util.List;

public class CompteBancaireView {

	AgenceView agenceView = new AgenceView();
	ClientView clientView = new ClientView();

	public void afficherComptesBancaires(List<CompteBancaire> comptesBancaires) {
		System.out.println(System.lineSeparator());
		System.out.printf("%-20s|", "Numero_compte");
		System.out.printf("%-20s|", "Code_agence");
		System.out.printf("%-20s|", "Id_client ");
		System.out.printf("%-10s|", "Type");
		System.out.printf("%-20s|", "Solde");
		System.out.printf("%-20s|", "Decouvert autorise");
		System.out.println();
		System.out.print("________________________________________________________________________________________________________________________________");
		System.out.println();
		for (int i = 0; i < comptesBancaires.size(); i++) {
			System.out.printf("%-20s|", comptesBancaires.get(i).getNumeroCompte());
			System.out.printf("%-20s|", comptesBancaires.get(i).getAgence().getCode());
			System.out.printf("%-20s|", comptesBancaires.get(i).getClient().getIdClient());
			System.out.printf("%-10s|", comptesBancaires.get(i).getType());
			System.out.printf("%-20s|", comptesBancaires.get(i).getSolde());
			System.out.printf("%-20s|", comptesBancaires.get(i).getDecouvertEstAutorise());
			System.out.println();
		}
		System.out.println(System.lineSeparator());
	}


	public String recupererNumeroCompteBancaire() {

		System.out.println(" Saisir le numero de compte a rechercher : ");
		String NumCompteSaisie = Constante.scanner.nextLine();
		NumCompteSaisie = ExceptionUtils.checkValue(NumCompteSaisie, Constante.scanner).trim();
		return NumCompteSaisie;

	}

	public void afficherCompte(CompteBancaire compte) {
		System.out.println(System.lineSeparator());
		System.out.printf("%-20s|", "Numero_compte");
		System.out.printf("%-20s|", "Code_agence");
		System.out.printf("%-20s|", "Id_client ");
		System.out.printf("%-10s|", "Type");
		System.out.printf("%-20s|", "Solde");
		System.out.printf("%-20s|", "Decouvert autorise");
		System.out.println();
		System.out.print("______________________________________________________________________________________________________________________");
		System.out.println();
		System.out.printf("%-20s|", compte.getNumeroCompte());
		System.out.printf("%-20s|", compte.getAgence().getCode());
		System.out.printf("%-20s|", compte.getClient().getIdClient());
		System.out.printf("%-10s|", compte.getType());
		System.out.printf("%-20s|", compte.getSolde());
		System.out.printf("%-20s|", compte.getDecouvertEstAutorise());
		System.out.println();
	}


	public String recupererTypeCompte() {
		String type = null;
		System.out.println("Taper: " + "\n" +
				"1 =>  Compte Courant " + "\n" +
				"2 =>  Plan d E pargne " + "\n" +
				"3 =>  Livret A");
		String choice = Constante.scanner.nextLine();
		if("1".equals(choice)) {
			type=  Constante.CC;;
		}
		else if("2".equals(choice)) {
			type=  Constante.PEL;
		}
		else if("3".equals(choice)) {
			type=  Constante.LA;
		}
		return type;
	}


	public Boolean decouvertAutorise() {
		 Boolean decouvert= null;
		System.out.println("Autorisez vous le decouvert   " + "\n" +
				"1 =>  oui " + "\n" +
				"2 =>  non " + "\n" );
		String choice = Constante.scanner.nextLine();

		if("1".equals(choice)) {
			decouvert= true;
		}
		else if("2".equals(choice)) {
			decouvert= false;
		}
		return decouvert;
	}

	public double recupererSolde() {
		System.out.println("Veuillez saisir le solde : " );
		double solde = Double.parseDouble(Constante.scanner.nextLine());
		 return solde;
	}
}
