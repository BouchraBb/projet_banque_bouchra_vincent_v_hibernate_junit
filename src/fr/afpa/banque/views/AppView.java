package fr.afpa.banque.views;

import fr.afpa.banque.util.Constante;

public class AppView {
    public String  menu() {
        System.out.println("-------------------------------    MENU BANQUE CDA JAVA   ------------------------------------------------");
        System.out.println("\t [1]- Creer une agence  ");
        System.out.println("\t [2]- Creer un client ");
        System.out.println("\t [3]- Creer un compte bancaire");
        System.out.println("\t [4]- Recherche de compte(numero de compte)");
        System.out.println("\t [5]- Recherche de client(Nom, Numero de compte, identifiant de client)");
        System.out.println("\t [6]- Afficher la liste des comptes d'un client(identifiant client)");
        System.out.println("\t [7]- Imprimer les infos client (identifiant client)");
        System.out.println("\t [8]- Quitter ");
        System.out.println("----------------------------------------------------------------------------------------------------------");
        System.out.println("Votre choix : ");
        System.out.println("Veuillez faire un choix :");
        String choice = Constante.scanner.nextLine();
         return choice;
    }

    public String  subMenu(){
        System.out.println("\t [5- A]- Recherche de client(Nom)");
        System.out.println("\t [5- B]- Recherche de client(Numero de compte)");
        System.out.println("\t [5- C]- Recherche de client(identifiant de client)");
        System.out.println("Veuillez faire un choix : ");
        String subChoice = Constante.scanner.nextLine().toUpperCase();
         return subChoice;
    }
     public void showMessage(String message ){
         System.out.println(message );
     }
}