package fr.afpa.banque.dao;

import fr.afpa.banque.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class GenericDAO<T> {
    public static Session session =  HibernateUtil.getSessionFactory().openSession();
    private Class<T> entity;
    public GenericDAO(Class<T> entity) {
        this.entity = entity;
    }

    public String save(T obj) {
        Transaction transaction = null;
        String id= null;
        try  {
            transaction = session.beginTransaction();
             id  = (String)session.save(obj);
            System.out.println("DEBUG "+ obj);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                System.out.println("error create");
                e.printStackTrace();
                transaction.rollback();
            }
        }
         return id;
    }

//    public void update(T obj)  {
//        Transaction tx = null;
//        try {
//            tx = session.beginTransaction();
//            session.saveOrUpdate(obj);
//            tx.commit();
//        } catch (Exception e) {
//            if (tx != null){
//                System.out.println("error update");
//                e.printStackTrace();
//                tx.rollback();
//            }
//
//        }
//    }

    public void delete(T obj)  {
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(obj);
            tx.commit();
        } catch (Exception e) {
            if (tx != null){
                System.out.println("error delete");
                tx.rollback();
            }

        }
    }

//    public void delete(T obj)  {
//        Transaction tx = null;
//        try {
//            tx = session.beginTransaction();
//            session.delete(obj);
//            tx.commit();
//        } catch (Exception e) {
//            if (tx != null){
//                System.out.println("error delete");
//                tx.rollback();
//            }
//
//        }
//    }

    public T findById(String id) {

        return (T) session.get(entity, id);
    }

    public List<T> findAll() {
        return (List<T>) session.createQuery("from " + entity.getName()).list();

    }


}
