package fr.afpa.banque.dao;

import fr.afpa.banque.model.Client;
import org.hibernate.Query;
import org.hibernate.Transaction;

import java.util.List;

public class ClientDAO extends GenericDAO<Client>{
    public ClientDAO() {
        super(Client.class);
    }

//    public Client findByIdString(String id) {
//        Transaction transaction = session.beginTransaction();
//        Query query = session.getNamedQuery("findByIdString");
//        query.setParameter("idClient", id);
//        Client client  = (Client) query.uniqueResult();
//        transaction.commit();
//        return client;
//
//    }

    public List<Client> findByName(String name){
        Query query = session.getNamedQuery("findByName");
        query.setParameter("nom", name);
        return (List<Client>) query.list();
    }

    public List<Client> findByNumeroDeCompte(String numeroCompte){
        Query query = session.getNamedQuery("findByNumeroDeCompte");
        query.setParameter("numeroCompte", numeroCompte);
        return (List<Client>) query.list();
    }

    public List<Client> findClientByIdClient(String idClient){
        Query query = session.getNamedQuery("findClientByIdClient");
        query.setParameter("idClient", idClient);
        return (List<Client>) query.list();
    }

   // for test
   public List<Client> findByNumeroDeCompte1(String numeroCompte) {
        Query query = session.getNamedQuery("findClientByNumCompte");
        query.setParameter("numeroCompte", numeroCompte);
        return (List<Client>) query.list();
    }
}
