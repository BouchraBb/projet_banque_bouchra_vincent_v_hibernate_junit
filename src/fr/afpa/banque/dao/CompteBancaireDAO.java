package fr.afpa.banque.dao;

import fr.afpa.banque.model.Client;
import fr.afpa.banque.model.CompteBancaire;
import org.hibernate.Query;
import org.hibernate.Transaction;

import java.util.List;

public class CompteBancaireDAO extends GenericDAO<CompteBancaire> {
    public CompteBancaireDAO() {
        super(CompteBancaire.class);
    }

    public CompteBancaire findByNumCompte(Integer num) {
        Transaction transaction = session.beginTransaction();
        Query query = session.getNamedQuery("findByNumCompte");
        query.setParameter("numeroCompte", num);
        CompteBancaire compteBancaire  = (CompteBancaire) query.uniqueResult();
        transaction.commit();
        return compteBancaire;
    }

    public CompteBancaire findCompteByNum(String num){
        Query query = session.getNamedQuery("findCompteByNum");
        query.setParameter("numeroCompte", num);
        CompteBancaire compteBancaire  = (CompteBancaire) query.uniqueResult();
        return compteBancaire;
    }

    public List<CompteBancaire> findCompteByIdClient(String idClient){
        Query query = session.getNamedQuery("findCompteByIdClient");
        query.setParameter("idClient", idClient);
        return (List<CompteBancaire>) query.list();
    }

    public CompteBancaire findeCompteSameType(Client client, String type) {
        Query query = session.getNamedQuery("findeCompteSameType");
        query.setParameter("type", type);
        query.setParameter("idClient", client.getIdClient());
        CompteBancaire compteBancaire  = (CompteBancaire) query.uniqueResult();
        return compteBancaire;
    }
}
