package fr.afpa.banque.config.test;

import fr.afpa.banque.dao.GenericDAO;
import fr.afpa.banque.model.Agence;
import fr.afpa.banque.service.AgenceService;
import fr.afpa.banque.util.HibernateUtilTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AgenceTest {
    Agence agence;
    @BeforeAll
    public static void instanceSession(){
        GenericDAO.session = HibernateUtilTest.getSessionFactory().openSession();
    }

    @BeforeEach
    public void setup(){ agence = new Agence();}

    AgenceService agenceService = new AgenceService();

    static String id;

    @Test
    public void create(){
        //given
        agence.setAdresse("131 rue du loup");
        agence.setNom("agence");
        //when
        id = agenceService.create(agence);

        Agence agenceRecovering = (Agence) GenericDAO.session.get(Agence.class, id);
        System.out.println(agenceRecovering.genererCode());

        //Verify
        Assertions.assertNotNull(agenceRecovering);
        Assertions.assertEquals(agence.getCode(), agenceRecovering.getCode());

    }

    @Test
    public void findById(){
        Agence agenceRecovering = agenceService.find(id);
        Assertions.assertNotNull(agenceRecovering);
    }

}
