package fr.afpa.banque.config.test;

import fr.afpa.banque.dao.GenericDAO;
import fr.afpa.banque.model.Client;
import fr.afpa.banque.service.ClientService;
import fr.afpa.banque.util.HibernateUtilTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Date;

public class ClientTest {
    @BeforeAll
    public static void instanceSession(){
        GenericDAO.session = HibernateUtilTest.getSessionFactory().openSession();
    }
    ClientService clientService = new ClientService();
    static String id;


    @Test
    public void create (){
        // given
         Client client= new Client("tata", "toto", Date.valueOf("1989-02-02"), "tata@gmail.com");
        //when
         String idClient = clientService.create(client);

       // System.out.println(id);
        Client clientFind = (Client) GenericDAO.session.get(Client.class, idClient);
        System.out.println(clientFind.getIdClient());
        id= client.getIdClient();

        // verifier
        Assertions.assertNotNull(clientFind);
        Assertions.assertEquals(client.getIdClient(), clientFind.getIdClient());

    }

    @Test
    public void finById(){
        //given
       // String idF = id  ;
        // when
        Client client= clientService.find(id);

    }

//    @Test
//    public void delete (){
//        // given
//       //id ="QX192723" ;
//
//        //when
//        Client clientFind = (Client) GenericDAO.session.get(Client.class, id);
//        Assertions.assertNotNull(clientFind);
//        System.out.println(clientFind.getIdClient());
//
//        clientService.delete(clientFind);
//        Client clientExist = (Client) GenericDAO.session.get(Client.class, id);
//
//        // verifier
//        Assertions.assertNull(clientExist);
//
//    }

}
