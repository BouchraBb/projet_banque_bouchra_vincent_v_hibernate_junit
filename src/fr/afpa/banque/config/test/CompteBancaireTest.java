package fr.afpa.banque.config.test;

import fr.afpa.banque.dao.GenericDAO;
import fr.afpa.banque.model.Agence;
import fr.afpa.banque.model.Client;
import fr.afpa.banque.model.CompteBancaire;
import fr.afpa.banque.service.AgenceService;
import fr.afpa.banque.service.ClientService;
import fr.afpa.banque.service.CompteBancaireService;
import fr.afpa.banque.util.Constante;
import fr.afpa.banque.util.HibernateUtilTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.sql.Date;

public class CompteBancaireTest {
    ClientService clientService = new ClientService();
    AgenceService agenceService = new AgenceService();
    CompteBancaireService  compteBancaireService = new CompteBancaireService();
    static String id ;

    @BeforeAll
    public static void instanceSession(){
        GenericDAO.session = HibernateUtilTest.getSessionFactory().openSession();
    }



    @Test
    public void create (){
        // given
         String idC = ""  ;
         String idA = "";
         String type = Constante.CC;
         double solde = 1000.0;
         boolean decouvert = true;

        //when
        Client client= (Client) GenericDAO.session.get(Client.class, idC);
        Agence agence=(Agence) GenericDAO.session.get(Agence.class, idA);

        CompteBancaire compteBancaire = new CompteBancaire( agence,client, type, solde, decouvert);
        id= compteBancaireService.create(compteBancaire);

        CompteBancaire compteBancaireFind = (CompteBancaire) GenericDAO.session.get(CompteBancaire.class, id);

        // verifier
        Assertions.assertNotNull(compteBancaireFind);
        Assertions.assertEquals(compteBancaire.getNumeroCompte(), compteBancaireFind.getNumeroCompte());

    }

    @Test
    public void finById(){
        //given
        // String idF = id  ;
        // when
        CompteBancaire compteBancaireFind = compteBancaireService.find(id);

    }



}
