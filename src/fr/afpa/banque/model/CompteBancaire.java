package fr.afpa.banque.model;
import javax.persistence.*;
import java.util.Random;

@Entity
@NamedQueries({
		@NamedQuery( name="findeCompteSameType",
				     query="SELECT cb FROM CompteBancaire cb WHERE cb.type = :type and cb.client.idClient= :idClient"),
		@NamedQuery( name="findCompteByNum",
					 query="SELECT cb FROM CompteBancaire cb WHERE cb.numeroCompte = :numeroCompte"),
		@NamedQuery(name="findCompteByIdClient",
		query="SELECT cb FROM CompteBancaire cb WHERE cb.client.idClient = :idClient")
})

@Table(uniqueConstraints = {@UniqueConstraint(columnNames = { "numeroCompte" })})
public class CompteBancaire {
	@Id
	private String numeroCompte ;
	private String type;
	private Double solde;
	private Boolean decouvertEstAutorise;
	@ManyToOne
	private Client client;
	@ManyToOne
	private Agence agence;
	public CompteBancaire() {
	}
	public CompteBancaire(Agence agence, Client client , String type, Double solde,
                          Boolean decouvertEstAutorise) {
		this.numeroCompte = genererNumeroCompte();
		this.agence = agence;
		this.client = client;
		this.type = type;
		this.solde = solde;
		this.decouvertEstAutorise = decouvertEstAutorise;
	}
	
	public String getNumeroCompte() {
		return this.numeroCompte;
	}
	public void setNumeroCompte() {
		this.numeroCompte = genererNumeroCompte() ;
	}


	public void setNumeroCompte(String numeroCompte) {
		this.numeroCompte = numeroCompte;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Agence getAgence() {
		return agence;
	}

	public void setAgence(Agence agence) {
		this.agence = agence;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public Double getSolde() {
		return solde;
	}
	public void setSolde(Double solde) {
		this.solde = solde ;
	}
	public Boolean getDecouvertEstAutorise() {
		return decouvertEstAutorise;
	}
	public void setDecouvertEstAutorise(Boolean decouvertEstAutorise) {
		this.decouvertEstAutorise = decouvertEstAutorise;
	}

	public String genererNumeroCompte() {
		Random run = new Random();
			int  runChiffres = (run.nextInt(9999999+1-1000000)+1000000);
			String numCompte = "2022"+ runChiffres;
			return numCompte;

		}

	@Override
	public String toString() {
		return "CompteBancaire{" +
				", numeroCompte='" + numeroCompte + '\'' +
				", type='" + type + '\'' +
				", solde=" + solde +
				", decouvertEstAutorise=" + decouvertEstAutorise +
				", client=" + client +
				", agence=" + agence +
				'}';
	}
}
