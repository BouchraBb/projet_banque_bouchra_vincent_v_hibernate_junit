package fr.afpa.banque.model;
import fr.afpa.banque.util.Constante;
import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Entity
@NamedQueries({
		@NamedQuery(name="findByIdString",
		query="SELECT c FROM Client c WHERE c.idClient = :idClient "),
		@NamedQuery(name="findByName",
		query="SELECT c FROM Client c WHERE c.nom = :nom"),
		@NamedQuery(name="findClientByIdClient",
		query="SELECT c FROM Client c WHERE c.idClient = :idClient"),
		@NamedQuery(name="findClientByNumCompte",
				query="SELECT c FROM Client c, CompteBancaire cb  WHERE  c.idClient=cb.client.idClient and cb.numeroCompte = :numeroCompte"),

})
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = { "idClient" })})
public class Client {
	@Id
	private String idClient;
	private String nom;
	private String prenom; 
	private Date dateDeNaissance;
	private String email;
	@ManyToOne
	private Agence agence;
	@OneToMany(mappedBy = "client", cascade = CascadeType.PERSIST)
	private List<CompteBancaire> comptes = new ArrayList<>();

	public Client() {
	}

	public Client(String nom, String prenom, Date dateDeNaissance, String email){
		this.idClient=genererIdClient();
		this.nom = nom;
		this.prenom = prenom;
		this.dateDeNaissance = dateDeNaissance;
		this.email = email;
	}

	public Agence getAgence() {
		return agence;
	}

	public void setAgence(Agence agence) {
		this.agence = agence;
	}

	public List<CompteBancaire> getComptes() {
		return comptes;
	}

	public void setComptes(List<CompteBancaire> comptes) {
		this.comptes = comptes;
	}

	public String getId() {
		return idClient;
	}

	public void setId() {
		this.idClient = genererIdClient();
	}

	private String genererIdClient(){
		Random run = new Random();
		char maj1 =(char)((run.nextInt(90+1-65)) +65 );
		char maj2 = (char)((run.nextInt(90+1-65)) +65 );
		String runMaj =""+ maj1 + maj2;
		String runChiffres = "" + (run.nextInt(999999+1-100000)+100000);
		String idClient = ""+ runMaj + runChiffres;
		return idClient;

	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getIdClient() {
		return idClient;
	}

	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}


	public Date getDateDeNaissance() {
		return dateDeNaissance;
	}

	public void setDateDeNaissance(Date dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Client{" +
				"idClient=" + idClient +
				", nom='" + nom + '\'' +
				", prenom='" + prenom + '\'' +
				", dateDeNaissance=" +  Constante.formatter.format( dateDeNaissance.toLocalDate()) +
				", email='" + email + '\'' +
				'}';
	}


}
