package fr.afpa.banque.model;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


@Entity
public class Agence {
		@Id
		@Column(unique = true)
		private String code;
		private String nom;
		private String adresse;

		@OneToMany(mappedBy = "agence", cascade = CascadeType.PERSIST)
		List<Client> clients = new ArrayList<>();

		@OneToMany(mappedBy = "agence", cascade = CascadeType.PERSIST)
		List<CompteBancaire> comptes = new ArrayList<>();

		public Agence() {
			this.code= genererCode();
		}
		public Agence(  String nom, String adresse) {
			this.code= genererCode() ;
			this.nom = nom;
			this.adresse = adresse;
		}

		public String  genererCode(){
			Random run = new Random();
			String codeGenerer = (run.nextInt(999+1-100)+100)+"";
			 return codeGenerer;
		}

	public List<Client> getClients() {
		return clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public List<CompteBancaire> getComptes() {
		return comptes;
	}

	public void setComptes(List<CompteBancaire> comptes) {
		this.comptes = comptes;
	}

	public void addCompte(CompteBancaire compteBancaire){
			this.getComptes().add(compteBancaire);
			compteBancaire.setAgence(this);
	}
	public void addClient(Client client){
		this.getClients().add(client);
		client.setAgence(this);
	}
	public void removeCompte(CompteBancaire compteBancaire){
		this.getComptes().remove(compteBancaire);
	}
	public void removeClient(Client client){
		this.getClients().remove(client);
	}

	public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code =code;
		}

		public String getNom() {
			return nom;
		}

		public void setNom(String nom) {
			this.nom = nom;
		}

		public String getAdresse() {
			return adresse;
		}

		public void setAdresse(String adresse) {
			this.adresse = adresse;
		}
		
		public String toString() {
			return "Agence [code agence=" + code + ", nom agence=" + nom + ", adresse=" + adresse + "]";
		}
		
	}
