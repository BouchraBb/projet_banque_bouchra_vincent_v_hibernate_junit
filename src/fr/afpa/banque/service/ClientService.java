package fr.afpa.banque.service;

import fr.afpa.banque.dao.ClientDAO;
import fr.afpa.banque.model.Agence;
import fr.afpa.banque.model.Client;

import java.util.List;

public class ClientService {
     ClientDAO clientDAO = new ClientDAO();

    public String create(Client client){
        String id = clientDAO.save(client);
         return id;
    }
//    public void update(Client client){
//        clientDAO.update(client);
//    }

//    public void delete(Client client){
//        clientDAO.delete(client);
//    }

    public Client find(String  id){
        return clientDAO.findById(id);

    }
    public List<Client> findAll(){
        return clientDAO.findAll();
    }

    public List<Client> findByName(String name){
        return clientDAO.findByName(name);
    }

    public List<Client> findByNumeroDeCompte(String numeroCompte){
        //return clientDAO.findByNumeroDeCompte(numeroCompte);
        return clientDAO.findByNumeroDeCompte1(numeroCompte);
    }

    public List<Client> findClientByIdClient(String idClient){
        return clientDAO.findClientByIdClient(idClient);
    }

    public Client findByIdString(String idClient){return clientDAO.findById(idClient);}
}
