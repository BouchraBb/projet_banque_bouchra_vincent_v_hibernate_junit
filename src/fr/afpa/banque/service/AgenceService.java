package fr.afpa.banque.service;

import fr.afpa.banque.dao.AgenceDAO;
import fr.afpa.banque.model.Agence;

import java.util.List;

public class AgenceService {
    AgenceDAO agenceDAO = new AgenceDAO();
    public String  create(Agence agence){
        String id = agenceDAO.save(agence);
        return id;
    }
//    public void update(Agence agence){
//        agenceDAO.update(agence);
//    }

//    public void delete(Agence agence){
//        agenceDAO.delete(agence);
//
//    }
    public Agence find(String id ){
        return agenceDAO .findById(id ) ;

    }
    public List<Agence> findAll(){
         return agenceDAO.findAll();
    }
}
