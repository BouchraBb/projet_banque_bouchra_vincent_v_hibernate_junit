package fr.afpa.banque.service;

import fr.afpa.banque.dao.CompteBancaireDAO;
import fr.afpa.banque.model.Client;
import fr.afpa.banque.model.CompteBancaire;
import fr.afpa.banque.util.Constante;

import java.util.List;

public class CompteBancaireService {
     CompteBancaireDAO compteBancaireDAO = new CompteBancaireDAO();
    public String create(CompteBancaire compteBancaire){
        String id = compteBancaireDAO.save(compteBancaire);
         return id;
    }
//    public void update(CompteBancaire compteBancaire){
//
//        compteBancaireDAO.update(compteBancaire);
//    }

//    public void delete(CompteBancaire compteBancaire){
//        compteBancaireDAO.delete(compteBancaire);
//    }

    public CompteBancaire find(String id ){
        return compteBancaireDAO.findById(id);

    }
    public List<CompteBancaire> findAll(){
        return compteBancaireDAO.findAll();
    }

    public String findIdClientByNumeroDeCompte(){
        System.out.println("Veuillez entrer un numero de compte : ");
        String numeroDeCompte = Constante.scanner.nextLine();
        CompteBancaire compteBancaire = compteBancaireDAO.findCompteByNum(numeroDeCompte);
        String idClient = compteBancaire.getClient().getIdClient();
        return idClient;
    }

    public List<CompteBancaire> findCompteByIdClient(String idClient){
        return compteBancaireDAO.findCompteByIdClient(idClient);
    }

    public CompteBancaire findeCompteSameType(Client client, String type ) {
        return compteBancaireDAO.findeCompteSameType(client, type);
    }
}
